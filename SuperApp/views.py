from django.shortcuts import render
from SuperApp import models
from datetime import datetime

def use_render(request):
    return render(request, "index.html")

def list_users(request, uid=0):
    print "in list users: " + str(uid)
    name="mike"
    user_list = models.userschek.objects.all()
    userinfo = {}
    if uid > 0 :
        userinfo = (models.userschek.objects.filter(id = uid)).first()
    
    return render(request, "list_users.html",{
        "userlist":user_list,
                  "id":uid,
                  "name":name,
                  "userinfo":userinfo
                  })

def meetings(request, uid=0):
    name="mike"
    print "in meetings: "+ str(uid)
    meet_list = models.meeting.objects.all()
    meetinfo = {}
    if uid >  0 :
        meetinfo = (models.meeting.objects.filter(id = uid)).first()
    
    return render(request, "meetings.html",{"meetlist":meet_list,
                  "id":uid,
                  "name":name,
                  "meetinfo":meetinfo
                  })

def about(request):
    return render(request, "about.html")

# http://127.0.0.1:8000/add_event/?name=Lesson1&date=2016-04-16&userid=1,2,3&duration=01:00:00

def string_to_datetime(duration):
    return datetime.strptime('Jun 1 2005 '+ duration, '%b %d %Y %H:%M:%S')

def add_event(request):
    for param in ["name", "duration", "date", "userid"]:
        if param not in request.GET:
            return render(request, "index.html")
    print request.GET["name"] + request.GET["date"] + request.GET["userid"] +request.GET["duration"]
    new_meeting = models.meeting()
    new_meeting.name = request.GET["name"]
    new_meeting.date = request.GET["date"]
    
    duration = string_to_datetime(request.GET["duration"])
    user_id = request.GET["userid"]
    user_list = user_id.split(",")
    new_meeting.start_time = find_time(user_list,duration)
    new_meeting.stop_time = normal_time(sec(new_meeting.start_time) + sec(duration))
    new_meeting.save()

    for member in user_list:
        new_user_meeting = models.users_meeting()
        new_user_meeting.meeting_obj = new_meeting
        new_user_meeting.user_obj = (models.userschek.objects.get(id = member))
        print dir((models.userschek.objects.get(id = member)))
        new_user_meeting.save()

    return render(request, "add_event.html")

def sec(n):
    time = n.hour * 3600 + n.minute * 60 + n.second
    return time

def normal_time(n):
    hour = '%02d'% int(n / 3600)
    minute = '%02d'% int((n - n % 3600) % 60)
    second = '%02d'% int(n % 3600)
    return ':'.join((hour, minute, second))


def find_time(user_list, duration):
    s = 0
    crossing = "00:00:00"
    stop_crossing = "00:00:00"
    spisok = []
    for member in user_list:
         user_obj = (models.userschek.objects.get(id = member))
         start_time = user_obj.start_time
         stop_time = user_obj.stop_time
         list1 = []
         list2 = []
         list1.append(start_time)
         list1.append(member)
         list1.append(1)
         list2.append(stop_time)
         list2.append(member)
         list2.append(2)
         spisok.append(list1)
         spisok.append(list2)
    spisok.sort()
    
    for list in spisok:
        if (list[2] == 1):
            s = s + 1
            if (s == len(spisok)/2):
                crossing = list[0]
        if (list[2] == 2):
            if s == len(spisok)/2:
                stop_crossing = list[0]
                if (sec(stop_crossing) - sec(crossing) >= sec(duration)):
                     return crossing
                else:
                    return "00:00:00"


