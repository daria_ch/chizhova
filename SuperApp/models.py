from __future__ import unicode_literals

from django.db import models

class userschek(models.Model):
    first_name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    start_time = models.TimeField(default='00:00:00')
    stop_time = models.TimeField(default='00:00:00')
    id = models.AutoField(primary_key=True)

class meeting(models.Model):
    name = models.CharField(max_length=200)
    date = models.DateField(default='1970-01-01')
    start_time = models.TimeField(default='00:00:00')
    stop_time = models.TimeField(default='00:00:00')
    id = models.AutoField(primary_key=True)

class users_meeting(models.Model):
    meeting_obj = models.ForeignKey(meeting, on_delete = models.CASCADE)
    user_obj = models.ForeignKey(userschek, on_delete = models.CASCADE)

